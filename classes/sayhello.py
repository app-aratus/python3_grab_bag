#!/usr/bin/python3

class SayHello:

    # Simple class that returns a greeting
    def toPerson(name):
        base_greeting = 'Hello:  '
        greeting = base_greeting + name + '!'
        return greeting

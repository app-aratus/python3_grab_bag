#!/usr/bin/python3
import math
import os
import uuid
from classes.sayhello import *  # We're importing our other class.

# Fibonacci series function
def doFibonacci(size):
    print('Fibonacci series up to:  {}.'.format(size))
    a, b = 0, 1
    while a < size:
        print(a)
        a, b = b, a+b

# Append 0's to list and return results function.
def appendToList(size):
    results = list()
    item = 0
    for i in range(size):
        results.append(item)
    return results

# Reverse generator
def reverse(data):
    for index in range(len(data)-1, -1, -1):
        yield data[index]
        
def doMain():
    print('Starting doMain() function.')

    # See:  https://docs.python.org/3/tutorial/index.html
    print(' ')
    print('Starting The Python 3 Grab Bag.')
    print('This is a huge collection of useful things you might want to do in Python 3.')

    # This '#' is a comment

    # The simplest thing you might want to do is print a simple string.
    # in this format.
    print(' ')
    print('Hello world.')

    # Simple string.
    print(' ')
    hello_world = 'Hello world using a string variable.'
    print(hello_world)

    # Multi-lined String.
    print(' ')
    multi_lined_string = """This is an example of a string
    that has multiple lines in it and it might wrap"""
    print(multi_lined_string)

    # Multi-lined string another way.
    print(' ')
    long_line_string = ('This is example of how to '
                    'do long lined text.')
    print(long_line_string)

    # Show how to deal with escape.
    print(' ')
    show_escape = 'Be careful that you don\'t forget to escape single quotes.'
    print(show_escape)

    # Show newline.
    print(' ')
    show_newline = 'This is how you could do new lines.  \n-Option 1 \n-Option 2 \n-Option 3'
    print(show_newline)

    # Using the str.format, which is the new way.
    print(' ')
    print('{} and {} went up the hill.'.format('Jack', 'Jill'))

    # Old string formatting.  You may see this on older projects.
    print(' ')
    print('The value of pi is approximately %.9f' % math.pi)

    # Float also known as a decimal number.
    print(' ')
    value = 23.729063
    # Using old formatting
    print( 'Printing a float (also known as decimal number) using old formatting:  %.3f  ' % (value))
    print( 'Printing a float (also known as decimal number) using new formatting:  {}'.format(value))

    # print some characters in a string.
    print(' ')
    word = 'fabulous'
    print(word)
    # First character
    print('First character:            {}'.format(word[0]))
    print('Last character:             {}'.format(word[-1]))
    print('Second to last character:   {}'.format(word[-2]))
    print('First 3 characters:         {}'.format(word[0:3]))
    print('First 3 characters:         {}'.format(word[:3]))
    print('Last 3 characters:          {}'.format(word[-3:]))

    # String length.
    print(' ')
    super_duper = 'Super Duper'
    length = len(super_duper)
    print('The length of:  \'{}\' is:  {}.'.format(super_duper, length))

    # List 1 level of files.
    print(' ')
    print('List one level of files.')
    search_dir = './files/files_manipulation'
    for entry in os.scandir(search_dir):
        if entry.is_file():
            print('This is a file:          {}'.format(entry.name))
        elif entry.is_dir():
            print('This is a directory:     {}'.format(entry.name))
        else:
            print('This is something else:  {}'.format(entry.name))

    #print(' ')
    #print('List all levels of files.')
    #for subdir, dirs, files in os.walk(search_dir):
    #    for file in files:
    #        print('Directory:  {}  File:  {}'.format(subdir,file))

    # print all levels of files the best way.
    print(' ')
    print('List all levels of files best way.')
    for root, dirs, files in os.walk(search_dir, topdown=False):
        for name in files:
            print(os.path.join(root, name))
    for name in dirs:
        print(os.path.join(root, name))

    # Lists
    print(' ')
    # A list of square numbers.
    squares = [1, 4, 9, 16, 25]
    print('Lists.  Squares:  {}'.format(squares))

    # A bigger list of square numbers.
    print(' ')
    squares = squares + [36, 49, 64, 81, 100]
    print('Lists.  Squares concatenation:  {}'.format(squares))

    # Simple loop.
    # https://docs.python.org/3/tutorial/controlflow.html#break-and-continue-statements-and-else-clauses-on-loops
    print(' ')
    print('Starting a simple loop. ')
    for n in range(1, 10):
        print('    n={}'.format(n))

    # Loop for a list.
    print(' ')
    print('Starting a loop of a list: ')
    print('Squares list:  {}'.format(squares))
    for i in squares:
        print('    {}'.format(i))

    # Loop over list using enumeration.
    print(' ')
    print('Starting a loop of a list using enumerate: ')
    for i, val in enumerate(squares):
        print (i, ",",val)

    # Iterator.
    print(' ')
    letters = 'abcdefghijklmnopqrstuvwxyz'
    print('Iterator for:  {}.'.format(letters))
    it = iter(letters)
    print('next(it):  {}'.format(next(it)))
    print('next(it):  {}'.format(next(it)))
    print('next(it):  {}'.format(next(it)))
    print('next(it):  {}'.format(next(it)))

    print(' ')
    first_number  = 10
    second_number = 3
    added = (first_number + second_number)
    multiplied = (first_number * second_number)
    divided = (first_number / second_number)
    remainder = (first_number % second_number)
    to_power_of = (first_number ** second_number)
    print('First number:        {}  Second number:  {}.'.format(first_number, second_number))
    print('Added numbers:       {}.'.format(added))
    print('Multiplied numbers:  {}.'.format(multiplied))
    print('Divided numbers:     {}.'.format(divided))
    print('Remainder numbers:   {}.'.format(remainder))
    print('To Power of:         {}.'.format(to_power_of))

    # Calling a function.
    print(' ')
    doFibonacci(100)

    # Calling a function that returns something.
    print(' ')
    print('Append to list function, and return results.')
    list = appendToList(20)
    print(list)

    # Calling a generator that reverses a string.
    print(' ')
    string_of_data = 'fred'
    print('Calling a generator that reverses a string:  {}.'.format(string_of_data))
    for char in reverse(string_of_data):
        print(char)

    # Call a function from another class file.
    # You would do this when your main Python script gets too big.
    print(' ')
    print('Calling a function from another class file.')
    name = 'jane'
    greeting = SayHello.toPerson(name)
    print(greeting)

    # Data Structures
    # Python Dictionary an implementation of a Hashtable
    print(' ')
    print('Using a Dictionary.')
    PhoneList = {}
    PhoneList['jane_smith_9273@hotmail.com'] = '+1-408-543-1234'
    PhoneList['john_doe_3717@gmail.com'] = '+1-415-489-9385'
    PhoneList['albert_sanchez@gmail.com'] = '+1-408-231-5672'
    PhoneList['zelda_lee@gmail.com'] = '+1-916-284-1693'
    PhoneList['sally_may_9922@gmail.com'] = '+1-415-554-9385'
    PhoneList['jacky_chen_3961@yahoo.com'] = '+1-916-255-9385'
    # A way to force possible shrinkage
    # https://mail.python.org/pipermail/python-list/2000-March/048085.html
    PhoneList = PhoneList.copy() 
    #for k in PhoneList.keys():
    #    print PhoneList[k]
    #for k,v in PhoneList.items():
    #    print k,':',v
    print(' ')
    print('PhoneList:  {}'.format(PhoneList))
    print(' ')
    print('PhoneList.keys():  {}'.format(PhoneList.keys()))
    print(' ')
    print('PhoneList.values():  {}'.format(PhoneList.values()))
    print(' ')
    print("PhoneList['jane_smith_9273@hotmail.com']:  {}".format(PhoneList['jane_smith_9273@hotmail.com']))
    print(' ')
    for k in PhoneList.keys():
        print('Key:  {}'.format(PhoneList[k]))
    print(' ')
    for item in PhoneList.items():
        print('Items:  {}'.format(item))
    print(' ')
    print('Unsorted keys ')
    for k,v in PhoneList.items():
        print('Key:  {}, Value:  {}'.format(k,v))
    print(' ')
    print('Sorted keys ')
    for k,v in sorted(PhoneList.items()):
        print('Key:  {}, Value:  {}'.format(k,v))

    # Random number. 
    print(' ')
    random = str(uuid.uuid4())
    print('Random number:  {}'.format(random))
    random = random.replace("-","") # Remove the UUID '-'.
    print('Random number:  {}'.format(random))
    random = random[-12:]
    print('Random number:  {}'.format(random))
    # 
    print(' ')

# This next line is the starting point of the program.
if __name__ == '__main__':
    # This calls the doMain function.
    doMain()


